from random import randint

guess_num = 1

for i in range(5):
    guess_month = randint(1, 12)
    guess_year = randint(1924, 2004)
    

    your_name = input("Hi! What is your name?")
    print("Guess", guess_num, ":", your_name,
     "were you born in", guess_month, "/", guess_year, "? ")
    is_birthday = input("yes or no? ")

    if is_birthday == "yes":
        print("I knew it!")
        break
    elif is_birthday == "no" and guess_num == 5:
        print("I have other things to do. Good bye.")
        break
    else: 
        print("Drat! Lemme try again!")
        guess_num += 1